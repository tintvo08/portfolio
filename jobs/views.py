import requests
import os
import json

from django.http import HttpResponse
from django.shortcuts import render
from dotenv import load_dotenv
load_dotenv()


def index(request):
    return render(request, 'jobs/index.html')


def home(request):
    return render(request, 'jobs/home.html')


def app(request):
    return render(request, 'jobs/app.html')


def aboutme(request):
    return render(request, 'jobs/aboutme.html')


def contact(request):
    return render(request, 'jobs/contact.html')


def search(request):
    key = {'key': os.getenv("TRADE_API_KEY")}
    return render(request, 'jobs/search.html', {'key': key})


def earnings(request):
    upcoming = _get_upcoming()
    stock_earnings = upcoming['earnings']
    return HttpResponse(json.dumps(stock_earnings), content_type="application/json")


def phases(request):
    upcoming = _get_upcoming()
    pharm_phases = upcoming['phases']
    return HttpResponse(json.dumps(pharm_phases), content_type="application/json")


def _get_upcoming():
    headers = {'api-key': os.getenv("TRADE_API_KEY")}
    resp = requests.get('https://t-trade-api.herokuapp.com/t_trade/upcoming', headers=headers)
    data = resp.json()
    return data


