# Summary
* This Repo contains my portfolio page
* The "My Apps" page uses a separated api and database to help me get information on the stocks I should be looking at
* I always integrated Google analytics to track it


<br>

### Portfolio Page: 
https://tin-profile.herokuapp.com/

### Run locally: 
python manage.py runserver