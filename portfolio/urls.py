from django.contrib import admin
from django.urls import path
import jobs.views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', jobs.views.index, name='jobs/index'),
    path('home', jobs.views.home, name='/jobs/home'),
    path('app', jobs.views.app, name='/jobs/app'),
    path('earnings', jobs.views.earnings, name='/jobs/earnings'),
    path('phases', jobs.views.phases, name='/jobs/phases'),
    path('aboutme', jobs.views.aboutme, name='/jobs/aboutme'),
    path('contact', jobs.views.contact, name='/jobs/contact')
]
